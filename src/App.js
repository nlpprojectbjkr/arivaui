
import {Menu,Container,Divider,Header} from 'semantic-ui-react';
import RecordVoice from './components/RecordVoice'
function App() {
  return (
    <div className="App">
      <Container>
       <Divider hidden />
        <div>
          <Menu borderless>
            <Container>
              <Menu.Item header>
                <Header>
                  {/* <Icon name='github' style={{ display: 'inline' }}></Icon> */}
                  Ariva
                </Header>
              </Menu.Item>
              <Menu.Item>
                <a href='/'>
                  Home
                </a>
              </Menu.Item>

              <Menu.Item>
                <a href='/'>
                  About
                </a>
              </Menu.Item>
            </Container>
          </Menu>
        </div>
        <Divider hidden></Divider>
        <div>
          <Container text>
            <RecordVoice/>
          </Container>
        </div>
        </Container>
    </div>
  );
}

export default App;
