import React, {useState} from 'react'
import {Header, Segment,Button, Grid, Divider} from 'semantic-ui-react'
import { ReactMic } from 'react-mic';
import ReactAudioPlayer from 'react-audio-player';

const RecordVoice = () => {
    const [record,setRecord] = useState(false)
    const [showPlay, setShowPlay] = useState(false)
    const [song,setSong] = useState('')
    const [isPressed,setIsPressed] = useState(false)

    const startRecording = () => {
        setRecord(true);
      }
     
    const stopRecording = () => {
        setRecord(false);

        setShowPlay(false);
        setIsPressed(!isPressed)
      }
     
    const onData = (recordedBlob)=>{
        // console.log('chunk of real-time data is: ', recordedBlob);
      }
     
    const onStop = (recordedBlob)=> {
        // console.log('recordedBlob is: ', recordedBlob);
        setShowPlay(true);
        setSong(recordedBlob.blobURL)

      }

    return (
        <div>
            <Segment padded="very" color="violet">
                <Grid>
                    <Grid.Row style={{ alignItems: 'center' }}>
                        <Grid.Column textAlign='center'>
                            <Header size='huge'>
                                Welcome to Ariva!
                            </Header>
                            <Header size='medium'>
                                Begin by recording your voice :
                                <ReactMic
                                    record={record}
                                    className="sound-wave"
                                    onStop={onStop}
                                    onData={onData}
                                    strokeColor="#6435C9"
                                    backgroundColor="#FFF" />
                            </Header>
                            <Divider hidden/>
                            {showPlay&&<ReactAudioPlayer
                            src={song}
                            controls
                            />}
                            <Divider hidden/>
                            <Button onClick={startRecording} type="button">Start</Button>
                            {!isPressed&&<Button onClick={stopRecording} type="button">Stop</Button>}
                            {isPressed&&<Button onClick={stopRecording} type="button">Reset</Button>}
                        </Grid.Column>
                    </Grid.Row>
                 </Grid>   
            </Segment>
        </div>
    )
}

export default RecordVoice
